# Contributor: Dhruvin Gandhi <contact@dhruvin.dev>
# Maintainer: Dhruvin Gandhi <contact@dhruvin.dev>
pkgname=hledger-iadd
pkgver=1.3.20
pkgrel=1
pkgdesc="A terminal UI as drop-in replacement for hledger add"
url="https://github.com/hpdeifel/hledger-iadd#readme"
arch="aarch64 x86_64" # limited by ghc
license="BSD-3-Clause"
makedepends="
	cabal
	ghc
	libffi-dev
	ncurses-dev
	zlib-dev
	"
_llvmver=15
options="net"
source="https://hackage.haskell.org/package/hledger-iadd-$pkgver/hledger-iadd-$pkgver.tar.gz
	ghc-9.8.patch
	cabal.project.freeze"

export CABAL_DIR="$srcdir"/cabal
export PATH="/usr/lib/llvm$_llvmver/bin:$PATH"

cabal_update() {
	cd "$builddir"
	cabal v2-update
	cabal v2-freeze --shadow-installed-packages
	mv cabal.project.freeze "$startdir"
}

prepare() {
	default_prepare
	cp "$srcdir"/cabal.project.freeze .
}

build() {
	cabal update
	cabal build --prefix=/usr --enable-relocatable
}

check() {
	cabal test
}

package() {
	install -Dm755 "$(cabal list-bin hledger-iadd)" "$pkgdir"/usr/bin/hledger-iadd
}

sha512sums="
0f59a39194eb0af1f9091518af48bbd5782882d65e0d7f567422473d686a663702f27027d443a007d95e690d7b155817667790533683e26d6a7dd11375efdd96  hledger-iadd-1.3.20.tar.gz
f1f99fd95f9d2f20e20dd9bd06536f8ad6f9bc0bbd755f57e7bdadb5fadc413dfa15c61757dc8308ec22f22db0319ad51e8e58db4d86bbe206c81f212d8dafbd  ghc-9.8.patch
8277639b63fe41b29e907f2a03121932fe814825a1442f0f749ad83afa0740d0b7a93c7d29463fbcbeb06e3dc9bc5a140b63e950bea47012e76d74a810e13bd7  cabal.project.freeze
"
