# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=py3-vt-py
pkgver=0.18.1
pkgrel=0
pkgdesc="Official Python client library for VirusTotal's REST API"
url="https://github.com/virustotal/vt-py"
license="Apache-2.0"
arch="noarch"
makedepends="py3-build py3-installer py3-setuptools py3-wheel py3-sphinx"
checkdepends="py3-pytest-asyncio py3-pytest-httpserver"
depends="py3-aiohttp"
source="$pkgname-$pkgver.tar.gz::https://github.com/VirusTotal/vt-py/archive/refs/tags/$pkgver.tar.gz"
subpackages="$pkgname-pyc $pkgname-doc"
builddir="$srcdir/vt-py-$pkgver"

build() {
	python3 -m build --wheel --skip-dependency-check --no-isolation
	sphinx-build -b man docs/source _build
}

check() {
	python3 -m venv --system-site-packages test-env
	test-env/bin/python -m installer dist/*.whl
	test-env/bin/python -m pytest
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
	install -Dm644 "_build/vt-py.1" -t "$pkgdir/usr/share/man/man1/"
}

sha512sums="
56282322beb12bf36a7d34dc2ae4d6423586f9d7daec774e007519eb8f5d38a67d14ccd370baf5fbb9099a3b1912247f8f03748097c4aa31dd5de31c18c24283  py3-vt-py-0.18.1.tar.gz
"
