# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Dave Hall <skwashd@gmail.com>
pkgname=composer
pkgver=2.7.3
pkgrel=0
pkgdesc="Dependency manager for PHP"
url="https://getcomposer.org/"
arch="noarch"
license="MIT"
_php=php83
depends="$_php $_php-phar $_php-curl $_php-iconv $_php-mbstring $_php-openssl $_php-zip"
checkdepends="git"
options="net"
source="$pkgname-$pkgver.phar::https://getcomposer.org/download/$pkgver/composer.phar"
subpackages="$pkgname-bash-completion"

# secfixes:
#   2.6.4-r0:
#     - CVE-2023-43655
#   2.3.5-r0:
#     - CVE-2022-24828
#   2.1.9-r0:
#     - CVE-2021-41116
#   2.0.13-r0:
#     - CVE-2021-29472

build() {
	$_php "$srcdir"/$pkgname-$pkgver.phar completion bash > "$srcdir"/$pkgname.bash
}

check() {
	cd "$srcdir"
	$_php $pkgname-$pkgver.phar -Vn
	$_php $pkgname-$pkgver.phar -n diagnose || true # fails as pub-keys are missing
}

package() {
	install -m 0755 -D "$srcdir"/$pkgname-$pkgver.phar "$pkgdir"/usr/bin/$pkgname.phar
	printf "#!/bin/sh\n\n/usr/bin/%s /usr/bin/composer.phar \"\$@\"\n" "$_php" \
		> "$pkgdir"/usr/bin/$pkgname
	chmod +x "$pkgdir"/usr/bin/$pkgname
	install -Dm644 "$srcdir"/$pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
}

sha512sums="
a7f8488f9fce418f3e3d7c5cdcb30a4357c4d3fc35f52a37675d295840b12c1ee0ff02319d44cfc6a9fb255f1728eacf0afa1965ef06db02682df0392783644b  composer-2.7.3.phar
"
